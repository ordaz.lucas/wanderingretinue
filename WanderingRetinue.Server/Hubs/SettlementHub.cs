﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Server.Helpers;

namespace WanderingRetinue.Server.Hubs
{
    [Authorize]
    public class SettlementHub : Hub
    {
        private Shared.Interfaces.IUserService _userService;
        public SettlementHub(Shared.Interfaces.IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        public async Task SendMessage(string user, string message)
        {
            var userAcct = this.GetUserAcct();

            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
