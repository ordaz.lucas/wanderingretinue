using AutoMapper;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OData.Edm;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WanderingRetinue.Server.Middleware;
using ProtoBuf.Grpc.Configuration;
using ProtoBuf.Grpc.Server;

namespace WanderingRetinue.Server
{
    public class Startup
    {

        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var dbConnectionString = _configuration.GetConnectionString("DefaultConnection");
            services.AddMvc().AddNewtonsoftJson();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });

            services.AddDbContext<ApiContext>(opt => opt.UseSqlServer(dbConnectionString));
            services.AddControllers();
            services.AddOData();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Wandering Retinue API", Version = "1" });
            });

            services.AddAutoMapper(typeof(Startup));

            // configure strongly typed settings objects
            var appSettingsSection = _configuration.GetSection("AppSettings");
            services.Configure<Helpers.AppSettings>(appSettingsSection);

            services.AddCodeFirstGrpc();

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<Helpers.AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = RetinueAuthOptions.DefaultScheme;
                x.DefaultChallengeScheme = RetinueAuthOptions.DefaultScheme;
            }).AddRetinueAuth(options => { });
            //.AddJwtBearer(x =>
            //{
            //    x.Events = new JwtBearerEvents
            //    {
            //        OnTokenValidated = context =>
            //        {
            //            var userService = context.HttpContext.RequestServices.GetRequiredService<Interfaces.IUserService>();
            //            var userId = int.Parse(context.Principal.Identity.Name);
            //            var user = userService.GetById(userId);
            //            if (user == null)
            //            {
            //                // return unauthorized if user no longer exists
            //                context.Fail("Unauthorized");
            //            }
            //            return Task.CompletedTask;
            //        },
            //        OnMessageReceived = context =>
            //        {

            //            var accessToken = context.Request.Query["access_token"];

            //            // If the request is for our hub...
            //            var path = context.HttpContext.Request.Path;
            //            if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/hub"))
            //            {
            //                // Read the token out of the query string
            //                context.Token = accessToken;
            //            }
            //            return Task.CompletedTask;
            //        }
            //    };
            //    x.RequireHttpsMetadata = false;
            //    x.SaveToken = true;
            //    x.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(key),
            //        ValidateIssuer = false,
            //        ValidateAudience = false
            //    };
            //});

            
            services.AddSignalR();
            services.AddScoped<Shared.Interfaces.IUserService, Services.UserService>();
            services.AddScoped<Services.NotificationService>();

            services.AddMvc(o =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                o.Filters.Add(new AuthorizeFilter(policy));
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
                
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseGrpcWeb();

            app.UseEndpoints(routes =>
            {
                routes.Select()
                    .Expand()
                    .Filter()
                    .OrderBy()
                    .MaxTop(100)
                    .Count();

                routes.MapDefaultControllerRoute();
                routes.MapODataRoute("odata", "odata", GetEdmModel());
                routes.MapHub<Hubs.SettlementHub>("/hub/settlement");
                routes.MapFallbackToFile("index.html");
                routes.MapGrpcService<Services.SettlementService>().EnableGrpcWeb();
                routes.MapGrpcService<Services.UserService>().EnableGrpcWeb();
                routes.MapGrpcService<Services.NotificationService>().EnableGrpcWeb();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "Wandering Retinue API (V1)");
            });

            var apiContext = serviceProvider.GetService<ApiContext>();
            var userService = serviceProvider.GetService<Shared.Interfaces.IUserService>();
            AddTestData(apiContext, userService);
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();
        }

        static IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();

            // Base Models
            odataBuilder.EntitySet<Shared.Models.Nemesis>("Nemesis");
            odataBuilder.EntitySet<Shared.Models.SurvivalAction>("SurvivalActions");
            odataBuilder.EntitySet<Shared.Models.StoryEvent>("StoryEvents");
            odataBuilder.EntitySet<Shared.Models.LanternEvent>("LanternEvents");
            odataBuilder.EntitySet<Shared.Models.Location>("Locations");
            odataBuilder.EntitySet<Shared.Models.Principle>("Principles");
            odataBuilder.EntitySet<Shared.Models.Monster>("Monsters");
            odataBuilder.EntitySet<Shared.Models.Item>("Items");
            odataBuilder.EntitySet<Shared.Models.User>("Users");

            // Settlement Models
            odataBuilder.EntitySet<Shared.Models.Settlement>("Settlements");
            odataBuilder.EntitySet<Shared.Models.SettlementUser>("SettlementUsers");
            odataBuilder.EntitySet<Shared.Models.Survivor>("Survivors");
            odataBuilder.EntitySet<Shared.Models.SettlementSurvivalAction>("SettlementSurvivalActions");
            odataBuilder.EntitySet<Shared.Models.SettlementNemesis>("SettlementNemesis");
            odataBuilder.EntitySet<Shared.Models.SettlementStoryEvent>("SettlementStoryEvents");
            odataBuilder.EntitySet<Shared.Models.SettlementLanternEvent>("SettlementLanternEvents");
            odataBuilder.EntitySet<Shared.Models.SettlementLocation>("SettlementLocations");
            odataBuilder.EntitySet<Shared.Models.SettlementPrinciple>("SettlementPrinciples");
            odataBuilder.EntitySet<Shared.Models.SettlementMonster>("SettlementMonsters");
            odataBuilder.EntitySet<Shared.Models.SettlementItem>("SettlementItems");


            return odataBuilder.GetEdmModel();
        }

        public static void AddTestData(ApiContext context, Shared.Interfaces.IUserService userService)
        {
            var adminUser = context.Users.FirstOrDefault(x => x.UserName == "admin");
            if (adminUser == null)
            {
                var resp = userService.Register(new Shared.DTO.UserAuthenticateRequest()
                {
                    UserName = "admin",
                    Password = "admin"
                }).Result;
                adminUser = new Shared.Models.User()
                {
                    Id = resp.Id,
                    UserName = resp.UserName
                };
            }

            var adminSettlements = context.SettlementUsers.Where(x => x.UserId == adminUser.Id);

            if (adminSettlements.Count() == 0)
            {
                //context.AddRange(new Shared.Models.Settlement()
                //{
                //    Name = "Test Settlement 1",
                //    Description = "Long live the King"
                //}, new Shared.Models.Settlement()
                //{
                //    Name = "Test Settlement 2",
                //    Description = "Cannibals"
                //});

                //var settlements = context.Settlements.OrderBy(x => x.Id);

                //context.AddRange(new Shared.Models.Survivor()
                //{
                //    Name = "Survivor 1",
                //    SettlementId = settlements.First().Id
                //}, new Shared.Models.Survivor()
                //{
                //    Name = "Survivor 1",
                //    SettlementId = settlements.First().Id
                //}, new Shared.Models.Survivor()
                //{
                //    Name = "Survivor 2",
                //    SettlementId = settlements.Last().Id
                //}, new Shared.Models.Survivor()
                //{
                //    Name = "Survivor 3",
                //    SettlementId = settlements.Last().Id
                //});

                //context.AddRange(new Shared.Models.SettlementUser()
                //{
                //    UserId = adminUser.Id,
                //    SettlementId = settlements.First().Id
                //}, new Shared.Models.SettlementUser()
                //{
                //    UserId = adminUser.Id,
                //    SettlementId = settlements.Last().Id
                //});

                //context.SaveChanges();
            }
            
        }
    }
}
