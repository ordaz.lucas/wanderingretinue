﻿using Grpc.Core;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Net.Http.Headers;
using ProtoBuf.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Server.Helpers
{
    public static class Extensions
    {
        public static User GetUserAcct(this ODataController controller)
        {
            return ((RetinueIdentity)controller.User.Identity).User;
        }

        public static User GetUserAcct(this Hub hub)
        {
            return ((RetinueIdentity)hub.Context.User.Identity).User;
        }

        public static (User user, string token) GetUserAcct(this CallContext context)
        {
            var serverContext = context.ServerCallContext;
            var httpContext = serverContext.GetHttpContext();
            var identity = ((RetinueIdentity)httpContext.User.Identity);
            var user = identity?.User;
            var accessToken = httpContext.Request.Headers[HeaderNames.Authorization].ToString();
            return (user, accessToken?.Split(" ")[^1]);
        }
    }
}
