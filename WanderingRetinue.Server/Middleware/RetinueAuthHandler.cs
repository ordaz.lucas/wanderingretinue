﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Server.Middleware
{
    public static class AuthenticationBuilderExtensions
    {
        // Custom authentication extension method
        public static AuthenticationBuilder AddRetinueAuth(this AuthenticationBuilder builder, Action<RetinueAuthOptions> configureOptions)
        {
            // Add custom authentication scheme with custom options and custom handler
            return builder.AddScheme<RetinueAuthOptions, RetinueAuthHandler>(RetinueAuthOptions.DefaultScheme, configureOptions);
        }
    }

    public class RetinueAuthOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "Retinue";
        public string Scheme => DefaultScheme;
        public StringValues AuthKey { get; set; }
    }

    public class RetinueAuthHandler : AuthenticationHandler<RetinueAuthOptions>
    {

        private Shared.Interfaces.IUserService _userService;
        public RetinueAuthHandler(IOptionsMonitor<RetinueAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, Shared.Interfaces.IUserService userService)
            : base(options, logger, encoder, clock)
        {
            _userService = userService;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var ticket = GenerateAuthTicket(Context, _userService);
            if(ticket.Principal.Identity.IsAuthenticated)
            {
                return Task.FromResult(AuthenticateResult.Success(ticket));
            } else
            {
                return Task.FromResult(AuthenticateResult.Fail("Not a valid user."));
            }
        
        }

        public static AuthenticationTicket GenerateAuthTicket(HttpContext context, Shared.Interfaces.IUserService userService)
        {
            // Authenticate the User
            var (ua, claims) = AuthenticateUser(context, userService);
            var (claimsIdentity, authProperties) = GenerateRetinueIdentity(ua, claims);
            // Return the Ticket
            return new AuthenticationTicket(new ClaimsPrincipal(claimsIdentity), authProperties, "Retinue");
        }

        public static (RetinueIdentity Identity, AuthenticationProperties AuthProperties) GenerateRetinueIdentity(Shared.Models.User userAcct, IEnumerable<Claim> jwtClaims)
        {
            // Build the Authentication Properties
            AuthenticationProperties authProperties = new AuthenticationProperties
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = null,
                AllowRefresh = true,
                IsPersistent = false
            };
            // What are the default claims the user has
            List<Claim> claimCollection = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, userAcct?.UserName ?? "Anonymous"),
            };
            // Add Other claims here

            // Return the Claims Identity
            return (new RetinueIdentity(userAcct, jwtClaims), authProperties);
        }

        public static (Shared.Models.User acct, IEnumerable<Claim> jwtClaims) AuthenticateUser(HttpContext context, Shared.Interfaces.IUserService userService)
        {
            var authHeader = context.Request.Headers["Authorization"];
            string token = null;
            if(authHeader.Count > 0)
            {
                var authHeaderSplit = authHeader[0].Split(" ");
                if(authHeaderSplit[0] == "Bearer")
                {
                    token = authHeaderSplit[1];
                }
            }

            if(!string.IsNullOrEmpty(token))
            {
                var jwtClaims = RetinueIdentity.ParseClaimsFromJwt(token);
                var resp = userService.AuthenticateUserByToken(new Shared.DTO.UserAuthenticateRequest()
                {
                    Password = token
                })?.Result;

                // Auth User Here
                if (resp == null || resp?.User == null)
                {
                    return (null, null);
                } else
                {
                    return (resp?.User, jwtClaims);
                }

                
            } else
            {
                return (null, null);
            }
            
            
        }

    }


}
