﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WanderingRetinue.Server
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            this.Database.EnsureCreated();
            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shared.Models.User>()
                .HasIndex(u => u.UserName)
                .IsUnique();

            modelBuilder.Entity<Shared.Models.User>()
                .HasIndex(u => u.Email)
                .IsUnique();
        }

        public DbSet<Shared.Models.Settlement> Settlements { get; set; }
        public DbSet<Shared.Models.Survivor> Survivors { get; set; }
        public DbSet<Shared.Models.SettlementUser> SettlementUsers { get; set; }
        public DbSet<Shared.Models.SettlementSurvivalAction> SettlementSurvivalActions { get; set; }
        public DbSet<Shared.Models.SettlementNemesis> SettlementNemesis { get; set; }
        public DbSet<Shared.Models.SettlementStoryEvent> SettlementStoryEvents { get; set; }
        public DbSet<Shared.Models.SettlementLanternEvent> SettlementLaternEvents { get; set; }
        public DbSet<Shared.Models.SettlementLocation> SettlementLocations { get; set; }
        public DbSet<Shared.Models.SettlementPrinciple> SettlementPrinciples { get; set; }
        public DbSet<Shared.Models.SettlementMonster> SettlementMonsters { get; set; }
        public DbSet<Shared.Models.SettlementItem> SettlementItems { get; set; }
        public DbSet<Shared.Models.Nemesis> Nemesis { get; set; }
        public DbSet<Shared.Models.SurvivalAction> SurvivalActions { get; set; }
        public DbSet<Shared.Models.StoryEvent> StoryEvents { get; set; }
        public DbSet<Shared.Models.LanternEvent> LaternEvents { get; set; }
        public DbSet<Shared.Models.Location> Locations { get; set; }
        public DbSet<Shared.Models.Principle> Principles { get; set; }
        public DbSet<Shared.Models.Monster> Monsters { get; set; }
        public DbSet<Shared.Models.Item> Items { get; set; }
        public DbSet<Shared.Models.User> Users { get; set; }
        public DbSet<Shared.Models.UserToken> UserTokens { get; set; }
        public DbSet<Shared.Models.NotificationSubscription> NotificationSubscriptions { get; set; }

    }
}
