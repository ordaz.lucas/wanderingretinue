﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WanderingRetinue.Server.Helpers;
using WanderingRetinue.Server.Middleware;

namespace WanderingRetinue.Server.Controllers
{
    [Authorize(AuthenticationSchemes = RetinueAuthOptions.DefaultScheme)]
    public class UsersController : ODataController
    {
        private readonly ApiContext _apiContext;
        private readonly Shared.Interfaces.IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(ApiContext context, Shared.Interfaces.IUserService userService, IMapper mapper)
        {
            _apiContext = context;
            _userService = userService;
            _mapper = mapper;
        }

        [EnableQuery]
        [ODataRoute("Users({userId})")]
        public IActionResult Get([FromODataUri]int userId)
        {
            var currentUser = this.GetUserAcct();

            if (currentUser == null)
                return Unauthorized();

            var user = _apiContext.Users.FirstOrDefault(x => x.Id == userId);

            return Ok(_mapper.Map<Shared.Models.User>(user));
        }
    }
}