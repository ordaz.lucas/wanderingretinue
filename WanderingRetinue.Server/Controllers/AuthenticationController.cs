﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Server.Controllers
{
    
    public class AuthenticationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly Shared.Interfaces.IUserService _userService;
        private readonly Helpers.AppSettings _appSettings;
        public AuthenticationController(IMapper mapper, Shared.Interfaces.IUserService userService, IOptions<Helpers.AppSettings> appSettings)
        {
            _mapper = mapper;
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("~/api/authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]Shared.DTO.UserAuthenticateRequest model)
        {
            var authResponse = await _userService.Authenticate(model);

            if (authResponse == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            // return basic user info and authentication token
            return Ok(authResponse);
        }

        [AllowAnonymous]
        [HttpPost("~/api/register")]
        public async Task<IActionResult> Register([FromBody]Shared.DTO.UserAuthenticateRequest model)
        {
            try
            {
                // create user
                await _userService.Register(model);
                return Ok();
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}