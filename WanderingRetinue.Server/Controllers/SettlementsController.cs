﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtoBuf.Grpc;
using WanderingRetinue.Server.Helpers;
using WanderingRetinue.Server.Middleware;

namespace WanderingRetinue.Server.Controllers
{
    [Authorize(AuthenticationSchemes = RetinueAuthOptions.DefaultScheme)]
    public class SettlementsController : ODataController
    {
        private readonly ApiContext _apiContext;
        private readonly Shared.Interfaces.IUserService _userService;
        private readonly Shared.Interfaces.ISettlementService _settlementService;

        public SettlementsController(ApiContext context, Shared.Interfaces.IUserService userService)
        {
            _apiContext = context;
            _userService = userService;
        }

        #region Settlements

        [EnableQuery]
        [ODataRoute("Settlements")]
        public IActionResult Get()
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlements = _settlementService.GetSettlements();

            return Ok(settlements);
        }

        [ODataRoute("Settlements")]
        public async Task<IActionResult> Post(WanderingRetinue.Shared.Models.Settlement settlement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _apiContext.Add(settlement);
            await _apiContext.SaveChangesAsync();
            return Created(settlement);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})")]
        public IActionResult Get([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Survivors)
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement);
        }

        [ODataRoute("Settlements({settlementId})")]
        public async Task<IActionResult> Patch([FromODataUri] int settlementId, Delta<Shared.Models.Settlement> settlement)
        {

            var user = this.GetUserAcct();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == settlementId);

            if (entity == null || !entity.Users.Any(u => u.UserId == user.Id))
            {
                return NotFound();
            }
            settlement.Patch(entity);
            try
            {
                await _apiContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_apiContext.Settlements.Any(s => s.Id == settlementId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Updated(entity);
        }
        [ODataRoute("Settlements({settlementId})")]
        public async Task<IActionResult> Put([FromODataUri] int settlementId, Shared.Models.Settlement update)
        {

            var user = this.GetUserAcct();

            bool userInSettlment = await _apiContext.SettlementUsers.AnyAsync(u => u.UserId == user.Id);

            if(!userInSettlment)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (settlementId != update.Id)
            {
                return BadRequest();
            }
            _apiContext.Entry(update).State = EntityState.Modified;
            try
            {
                await _apiContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_apiContext.Settlements.Any(s => s.Id == settlementId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Updated(update);
        }

        #endregion Settlements

        #region Survivors

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Survivors")]
        public IActionResult GetSurvivors([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Survivors)
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Survivors);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Survivors({survivorId})")]
        public IActionResult GetSurvivor([FromODataUri]int settlementId, [FromODataUri]int survivorId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Survivors)
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Survivors.First(s => s.Id == survivorId));
        }

        #endregion Survivors

        #region Users

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Users")]
        public IActionResult GetSettlementUsers([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Users);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Users({userId})")]
        public IActionResult GetSettlementUsers([FromODataUri]int settlementId, [FromODataUri]int userId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Users.First(s => s.Id == userId));
        }

        #endregion Users

        #region SurvivalActions

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/SurvivalActions")]
        public IActionResult GetSurvivalActions([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.SurvivalActions);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/SurvivalActions({survivalActionId})")]
        public IActionResult GetSurvivalAction([FromODataUri]int settlementId, [FromODataUri]int survivalActionId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.SurvivalActions.First(s => s.Id == survivalActionId));
        }

        #endregion SurvivalActions

        #region Nemesis

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Nemesis")]
        public IActionResult GetNemesis([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Nemesis);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Nemesis({nemesisId})")]
        public IActionResult GetNemesis([FromODataUri]int settlementId, [FromODataUri]int nemesisId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Nemesis.First(s => s.Id == nemesisId));
        }

        #endregion Nemesis

        #region StoryEvents

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/StoryEvents")]
        public IActionResult GetStoryEvents([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.StoryEvents);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/StoryEvents({storyEventId})")]
        public IActionResult GetStoryEvent([FromODataUri]int settlementId, [FromODataUri]int storyEventId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.StoryEvents.First(s => s.Id == storyEventId));
        }

        #endregion StoryEvents

        #region LanternEvents

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/LanternEvents")]
        public IActionResult GetLaternEvents([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.LanternEvents);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/LanternEvents({lanternEventId})")]
        public IActionResult GetLanternEvent([FromODataUri]int settlementId, [FromODataUri]int lanternEventId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.LanternEvents.First(s => s.Id == lanternEventId));
        }

        #endregion LanternEvents

        #region Locations

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Locations")]
        public IActionResult GetLocations([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Locations);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Locations({locationId})")]
        public IActionResult GetLocation([FromODataUri]int settlementId, [FromODataUri]int locationId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Locations.First(s => s.Id == locationId));
        }

        #endregion Locations

        #region Principles

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Principles")]
        public IActionResult GetPrinciples([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Principles);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Principles({principleId})")]
        public IActionResult GetPrinciple([FromODataUri]int settlementId, [FromODataUri]int principleId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Principles.First(s => s.Id == principleId));
        }

        #endregion Principles

        #region Monsters

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Monsters")]
        public IActionResult GetMonsters([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Monsters);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Monsters({monsterId})")]
        public IActionResult GetMonster([FromODataUri]int settlementId, [FromODataUri]int monsterId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Monsters.First(s => s.Id == monsterId));
        }

        #endregion Monsters

        #region Items

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Items")]
        public IActionResult GetItems([FromODataUri]int settlementId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Items);
        }

        [EnableQuery]
        [ODataRoute("Settlements({settlementId})/Items({itemId})")]
        public IActionResult GetItem([FromODataUri]int settlementId, [FromODataUri]int itemId)
        {
            var user = this.GetUserAcct();

            if (user == null)
                return Unauthorized();

            var settlement = _apiContext.Settlements
                .Include(x => x.Users)
                .FirstOrDefault(s => s.Id == settlementId);

            if (!settlement.Users.Any(u => u.UserId == user.Id))
                return NotFound();

            return Ok(settlement.Items.First(s => s.Id == itemId));
        }

        #endregion Items

    }
}