﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.DTO;
using Grpc.Core;
using ProtoBuf.Grpc;
using Microsoft.AspNetCore.Authorization;
using WanderingRetinue.Server.Middleware;
using WanderingRetinue.Server.Helpers;
using WebPush;
using System.Text.Json;

namespace WanderingRetinue.Server.Services
{
    [Authorize(AuthenticationSchemes = RetinueAuthOptions.DefaultScheme)]
    public class NotificationService : Shared.Interfaces.INotificationService
    {
        private readonly ApiContext _apiContext;
        private readonly Shared.Interfaces.IUserService _userService;
        public NotificationService(ApiContext context, Shared.Interfaces.IUserService userService)
        {
            _apiContext = context;
            _userService = userService;
        }

        public Task Subscribe(Shared.Models.NotificationSubscription subscription, CallContext context = default)
        {
            var (user, _) = context.GetUserAcct();
            subscription.UserId = user.Id;
            _apiContext.NotificationSubscriptions.Add(subscription);
            _apiContext.SaveChanges();

            return Task.CompletedTask;
        }

        public async System.Threading.Tasks.Task SendNotification(Shared.Models.NotificationSubscription subscription, string message)
        {
            // For a real application, generate your own
            var publicKey = "BLC8GOevpcpjQiLkO7JmVClQjycvTCYWm6Cq_a7wJZlstGTVZvwGFFHMYfXt6Njyvgx_GlXJeo5cSiZ1y4JOx1o";
            var privateKey = "OrubzSz3yWACscZXjFQrrtDwCKg-TGFuWhluQ2wLXDo";

            var pushSubscription = new PushSubscription(subscription.Url, subscription.P256dh, subscription.Auth);
            var vapidDetails = new VapidDetails("mailto:<someone@example.com>", publicKey, privateKey);
            var webPushClient = new WebPushClient();
            try
            {
                var payload = JsonSerializer.Serialize(new
                {
                    message,
                    url = "/",
                });
                await webPushClient.SendNotificationAsync(pushSubscription, payload, vapidDetails);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error sending push notification: " + ex.Message);
            }
        }
    }
}
