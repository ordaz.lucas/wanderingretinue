﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.DTO;
using Grpc.Core;
using ProtoBuf.Grpc;
using Microsoft.AspNetCore.Authorization;
using WanderingRetinue.Server.Middleware;
using WanderingRetinue.Server.Helpers;

namespace WanderingRetinue.Server.Services
{
    [Authorize(AuthenticationSchemes = RetinueAuthOptions.DefaultScheme)]
    public class SettlementService : WanderingRetinue.Shared.Interfaces.ISettlementService
    {
        private readonly ApiContext _apiContext;
        private readonly Shared.Interfaces.IUserService _userService;
        private readonly Services.NotificationService _notificationService;
        public SettlementService(ApiContext context, Shared.Interfaces.IUserService userService, NotificationService notificationService)
        {
            _apiContext = context;
            _userService = userService;
            _notificationService = notificationService;
        }

        public async Task AddSettlement(Shared.Models.Settlement settlement, CallContext context = default)
        {
            var (user, _) = context.GetUserAcct();

            settlement.Users = new List<Shared.Models.SettlementUser>()
            {
                new Shared.Models.SettlementUser()
                {
                    UserId = user.Id
                }
            };

            _apiContext.Settlements.Add(settlement);
            await _apiContext.SaveChangesAsync();
        }

        public Task DeleteSettlement(Shared.Models.Settlement settlement, CallContext context = default)
        {
            throw new NotImplementedException();
        }

        
        public Task<GetSettlementsReply> GetSettlements(CallContext context)
        {
            var (user, _) = context.GetUserAcct();

            var settlements = _apiContext.Settlements
                .Include(x => x.Survivors)
                .Include(x => x.Users)
                .Where(x => x.Users.Any(y => y.UserId == user.Id))
                .ToList();

            return Task.FromResult(new GetSettlementsReply()
            {
                Settlements = settlements
            });
        }

        public Task UpdateSettlement(Shared.Models.Settlement settlement, CallContext context = default)
        {
            throw new NotImplementedException();
        }
    }
}
