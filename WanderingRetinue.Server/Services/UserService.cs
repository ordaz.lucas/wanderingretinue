﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProtoBuf.Grpc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WanderingRetinue.Server.Helpers;
using WanderingRetinue.Server.Middleware;
using WanderingRetinue.Shared.DTO;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Server.Services
{
    public class UserService : Shared.Interfaces.IUserService
    {
        private ApiContext _apiContext;
        private readonly Helpers.AppSettings _appSettings;

        public UserService(ApiContext context, IOptions<Helpers.AppSettings> appSettings)
        {
            _apiContext = context;
            _appSettings = appSettings.Value;
        }

        private string GenerateToken(int userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var expirationDate = DateTime.UtcNow.AddDays(7);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userId.ToString())
                }),
                Expires = expirationDate,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            var (tokenHash, tokenSalt) = CreatePasswordHash(token);

            _apiContext.UserTokens.Add(new Shared.Models.UserToken()
            {
                UserId = userId,
                ExpirationDate = expirationDate,
                TokenHash = tokenHash,
                TokenSalt = tokenSalt
            });

            _apiContext.SaveChanges();

            return token;
        }

        [AllowAnonymous]
        public Task<Shared.DTO.UserAuthenticateResponse> Authenticate(Shared.DTO.UserAuthenticateRequest req, ProtoBuf.Grpc.CallContext context)
        {

            var user = Authenticate(req.UserName, req.Password);
            var token = GenerateToken(user.Id);
            

            // authentication successful
            return Task.FromResult(new Shared.DTO.UserAuthenticateResponse()
            {
                UserName = user.UserName,
                Id = user.Id,
                Token = token
            });
        }

        public Shared.Models.User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _apiContext.Users.SingleOrDefault(x => x.UserName == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        [AllowAnonymous]
        public Task<Shared.DTO.UserAuthenticateResponse> AuthenticateUserByToken(Shared.DTO.UserAuthenticateRequest req, ProtoBuf.Grpc.CallContext context)
        {
            var jwtClaims = RetinueIdentity.ParseClaimsFromJwt(req.Password);
            var name = jwtClaims.FirstOrDefault(kvp => kvp.Type == "unique_name")?.Value;
            var userId = int.Parse(name);
            var tokens = _apiContext.UserTokens.Where(x => x.UserId == userId).ToList();

            if(tokens.Any(x => VerifyPasswordHash(req.Password, x.TokenHash, x.TokenSalt)))
            {
                var user = _apiContext.Users.SingleOrDefault(x => x.Id == userId);
                return Task.FromResult(new Shared.DTO.UserAuthenticateResponse()
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Token = req.Password,
                    User = user
                });
            } else
            {
                return null;
            }

        }

        public IEnumerable<Shared.Models.User> GetAll()
        {
            return _apiContext.Users;
        }

        public Task<Shared.Models.User> GetUser(int id)
        {
            var user = _apiContext.Users.Find(id);
            return Task.FromResult(user);
        }

        public Task<Shared.Models.User> GetUser(Shared.Models.User user, ProtoBuf.Grpc.CallContext context)
        {
            return GetUser(user.Id);
        }

        public Task<Shared.DTO.UserAuthenticateResponse> Register(Shared.DTO.UserAuthenticateRequest req, ProtoBuf.Grpc.CallContext context)
        {
            // validation
            if (string.IsNullOrWhiteSpace(req.Password))
                throw new Exception("Password is required");

            if (_apiContext.Users.Any(x => x.UserName == req.UserName))
                throw new Exception($"Username {req.UserName} is already taken");

            if (_apiContext.Users.Any(x => x.Email == req.Email))
                throw new Exception($"Email Address {req.Email} is already being used");

            var user = new Shared.Models.User()
            {
                UserName = req.UserName,
                Email = req.Email
            };

            var (passwordHash, passwordSalt) = CreatePasswordHash(req.Password);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _apiContext.Users.Add(user);
            _apiContext.SaveChanges();

            var token = GenerateToken(user.Id);

            return Task.FromResult(new Shared.DTO.UserAuthenticateResponse()
            {
                UserName = user.UserName,
                Id = user.Id,
                Token = token
            });
        }

        public void Update(Shared.Models.User userParam, string password = null)
        {
            var user = _apiContext.Users.Find(userParam.Id);

            if (user == null)
                throw new Exception("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.UserName) && userParam.UserName != user.UserName)
            {
                // throw error if the new username is already taken
                if (_apiContext.Users.Any(x => x.UserName == userParam.UserName))
                    throw new Exception("Username " + userParam.UserName + " is already taken");

                user.UserName = userParam.UserName;
            }

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                var (passwordHash, passwordSalt) = CreatePasswordHash(password);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _apiContext.Users.Update(user);
            _apiContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _apiContext.Users.Find(id);
            if (user != null)
            {
                _apiContext.Users.Remove(user);
                _apiContext.SaveChanges();
            }
        }


        private static (byte[] passwordHash, byte[] passwordSalt) CreatePasswordHash(string password)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                return (hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)), hmac.Key);
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        [Authorize(AuthenticationSchemes = RetinueAuthOptions.DefaultScheme)]
        public async Task Logout(ProtoBuf.Grpc.CallContext context = default)
        {
            var (user, httpToken) = context.GetUserAcct();
            var jwtClaims = RetinueIdentity.ParseClaimsFromJwt(httpToken);
            var name = jwtClaims.FirstOrDefault(kvp => kvp.Type == "unique_name")?.Value;
            var userId = int.Parse(name);

            var tokens = _apiContext.UserTokens.Where(x => x.UserId == userId).ToList();

            var token = tokens.FirstOrDefault(x => VerifyPasswordHash(httpToken, x.TokenHash, x.TokenSalt));

            if(token != null)
            {
                _apiContext.UserTokens.Remove(token);
                await _apiContext.SaveChangesAsync();
            }
        }
    }
}
