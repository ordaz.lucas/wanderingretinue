﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Simple.OData.Client;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace WanderingRetinue.Client.AuthenticationStateProviders
{
    public class TokenAuthenticationStateProvider : AuthenticationStateProvider
    {
        private const string _storageKey = "token";
        private class TokenStorage
        {
            public string Token { get; set; }
            public DateTime? Expiry { get; set; }
        }

        private readonly ISyncLocalStorageService _localStorage;
        private readonly NavigationManager _navigationManager;
        public AuthenticationState AuthenticationState { get; private set; }
        public Helpers.ClientBuilder ClientBuilder { get; private set; }
        

        public TokenAuthenticationStateProvider(ISyncLocalStorageService localStorage, NavigationManager navigationManager)
        {
            _localStorage = localStorage;
            _navigationManager = navigationManager;
            // Have to instantiate this manually, otherwise DI creates a cycle
            ClientBuilder = new Helpers.ClientBuilder(navigationManager, this);
        }

        public async Task SetTokenAsync(string token, DateTime expiry = default)
        {
            await Task.Run(() =>
            {
                if (token == null)
                {
                    _localStorage.SetItem(_storageKey, new TokenStorage
                    {
                        Token = null,
                        Expiry = null
                    });
                }
                else
                {
                    _localStorage.SetItem(_storageKey, new TokenStorage
                    {
                        Token = token,
                        Expiry = expiry
                    });
                }
                
                NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
            });

            await ClientBuilder.RebuildGrpcChannel();
            await ClientBuilder.RebuildODataClient();
        }

        public async Task<string> GetTokenAsync()
        {
            TokenStorage tokenObj = null;
            if(_localStorage.ContainKey(_storageKey))
            {
                tokenObj = _localStorage.GetItem<TokenStorage>(_storageKey);
            }

            if (tokenObj?.Expiry != null)
            {
                if (tokenObj?.Expiry > DateTime.Now)
                {

                    return tokenObj?.Token;
                }
                else
                {
                    await SetTokenAsync(null);
                }
            }
            return null;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await GetTokenAsync();
            ClaimsIdentity identity;
            if(string.IsNullOrEmpty(token))
            {
                identity = new ClaimsIdentity();
            } else
            {
                var jwtClaims = WanderingRetinue.Shared.Models.RetinueIdentity.ParseClaimsFromJwt(token);
                var userId = jwtClaims.FirstOrDefault(kvp => kvp.Type == "unique_name")?.Value;
                var userService = await ClientBuilder.BuildRPCClient<WanderingRetinue.Shared.Interfaces.IUserService>();
                try
                {
                    var user = await userService.GetUser(new WanderingRetinue.Shared.Models.User() { Id = int.Parse(userId) });
                    identity = new WanderingRetinue.Shared.Models.RetinueIdentity(user, jwtClaims);
                } catch(Exception e)
                {
                    identity = new ClaimsIdentity();
                }
                
            }
            AuthenticationState = new AuthenticationState(new ClaimsPrincipal(identity));
            return AuthenticationState;
        }

    }
}
