﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Client.Interfaces;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Client.Models
{
    public class SettlementState : Interfaces.ISettlementState
    {
        public SettlementState(IAppState appState)
        {
            AppState = appState;
        }

        public IAppState AppState { get; private set; }

        public Settlement Settlement { get; private set; }

        public async Task SetSettlement(int settlementId)
        {
            if (settlementId == 0) throw new ArgumentException("Argument cannot be 0", nameof(settlementId));
            var _client = await AppState.ClientBuilder.BuildODataClient();
            //Settlement = await _client.For<Settlement>()
            //    .Key(settlementId)
            //    .Expand(s => new
            //    {
            //        s.Survivors,
            //        s.Monsters,
            //        s.Nemesis,
            //        s.StoryEvents
            //    })
            //    .FindEntryAsync();
        }

        public async Task SetSettlement(Settlement settlement) => await SetSettlement(settlement?.Id ?? 0);

        public async Task UnsetSettlement()
        {
            await Task.Run(() =>
            {
                Settlement = null;
            });
        }
    }
}
