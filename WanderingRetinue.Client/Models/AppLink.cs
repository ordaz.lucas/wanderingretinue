﻿using System;

namespace WanderingRetinue.Client.Models
{
    public class AppLink
    {
        public enum Location
        {
            Top,
            Bottom
        }

        public AppLink(object owner, string name, System.Uri url, Location location, int order)
        {
            Owner = owner;
            Name = name;
            Url = url;
            NavLocation = location;
            Order = order;
        }

        public AppLink(object owner, string name, Action action, Location location, int order)
        {
            Owner = owner;
            Name = name;
            Action = action;
            NavLocation = location;
            Order = order;
        }

        public string Name { get; private set; }
        public System.Uri Url { get; private set; }
        public Location NavLocation { get; private set; }
        public object Owner { get; private set; }
        public int Order { get; private set; }
        public Action Action { get; private set; }
    }
}