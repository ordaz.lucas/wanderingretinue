﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;

namespace WanderingRetinue.Client.Shared
{
    public class MainLayoutBase : LayoutComponentBase
    {
        [CascadingParameter]
        Task<AuthenticationState> AuthenticationStateTask { get; set; }

        [Inject]
        public TokenAuthenticationStateProvider AuthStateProvider { get; set; }


        [Inject]
        public Interfaces.IAppState AppState { get; set; }

        public bool NavBarVisible = false;

        protected override void OnInitialized()
        {
            AppState.LocationTitleChanges += RefreshState;
            AppState.NavLinkChanges += RefreshState;
            base.OnInitialized();
        }

        protected override async Task OnInitializedAsync()
        {
            await AuthenticationStateTask;
            
        }

        public async System.Threading.Tasks.Task Logout()
        {
            await AppState.AuthStateProvider.SetTokenAsync(null);
        }

        void RefreshState(object sender, EventArgs e)
        {
            StateHasChanged();
        }

    }
}
