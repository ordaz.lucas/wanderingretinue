﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.Models;

namespace WanderingRetinue.Client.Shared.Components
{
    public class SettlementSummaryBase : ComponentBase
    {
        [Parameter]
        public Settlement Model { get; set; }

        [Inject]
        public NavigationManager NavManager { get; set; }

        protected void GoToSettlement()
        {
            NavManager.NavigateTo($"/settlement/{Model.Id}");
        } 
    }
}
