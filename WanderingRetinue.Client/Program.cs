﻿using Blazored.LocalStorage;
using Grpc.Net.Client;
using Grpc.Net.Client.Web;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MudBlazor.Services;
using ProtoBuf.Grpc.Client;
using Simple.OData.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;

namespace WanderingRetinue.Client
{
    public class Program
    {

        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            string baseAddress = builder.HostEnvironment.BaseAddress;
            var services = builder.Services;

            services.AddOptions();
            services.AddAuthorizationCore();
            services.AddScoped<TokenAuthenticationStateProvider>();
            services.AddScoped<AuthenticationStateProvider>(provider => provider.GetRequiredService<TokenAuthenticationStateProvider>());
            services.AddBlazoredLocalStorage();
            services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            services.AddMudServices();

            // Should be last for Injection
            services.AddScoped<Interfaces.IAppState, Services.AppState>();

            // Reference so the library doesn't get unlinked and not
            // included in the build
            Simple.OData.Client.V4Adapter.Reference();

            var host = builder.Build();

            await host.RunAsync();
        }
    }
}
