﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;
using WanderingRetinue.Client.Models;

namespace WanderingRetinue.Client.Interfaces
{
    public interface IAppState
    {
        TokenAuthenticationStateProvider AuthStateProvider { get; }
        Helpers.ClientBuilder ClientBuilder { get; }
        NavigationManager NavManager { get; }
        AuthenticationState AuthenticationState { get; }
        ClaimsPrincipal User { get; }

        event EventHandler NavLinkChanges;
        event EventHandler LocationTitleChanges;
        string Name { get; set; }
        string LocationTitle { get; }
        List<AppLink> NavLinks { get; }
        SettlementState SettlementState { get; }


        void SetNavLinks(List<AppLink> links);
        void SetUser(ClaimsPrincipal user);
        void SetLocationTitle(string location);
        
    }
}