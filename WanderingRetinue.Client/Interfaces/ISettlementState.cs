﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WanderingRetinue.Client.Interfaces
{
    public interface ISettlementState
    {
        WanderingRetinue.Shared.Models.Settlement Settlement { get; }
        Task SetSettlement(int settlementId);
        Task SetSettlement(WanderingRetinue.Shared.Models.Settlement settlement);
        Task UnsetSettlement();
    }
}
