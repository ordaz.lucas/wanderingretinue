using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;
using WanderingRetinue.Client.Interfaces;
using WanderingRetinue.Client.Models;

namespace WanderingRetinue.Client.Services
{
    public class AppState : IAppState
    {
        public AppState()
        {
            SettlementState = new SettlementState(this);
        }

        public AppState(TokenAuthenticationStateProvider tokenAuthenticationStateProvider, NavigationManager navigationManager) : this()
        {
            AuthStateProvider = tokenAuthenticationStateProvider ?? throw new ArgumentNullException(nameof(tokenAuthenticationStateProvider));
            NavManager = navigationManager;
        }

        public TokenAuthenticationStateProvider AuthStateProvider { get; private set; }
        public Helpers.ClientBuilder ClientBuilder
        {
            get
            {
                return AuthStateProvider.ClientBuilder;
            }
        }
        public NavigationManager NavManager { get; private set; }
        public AuthenticationState AuthenticationState => AuthStateProvider?.AuthenticationState;

        public ClaimsPrincipal User => AuthenticationState?.User;


        public event EventHandler NavLinkChanges;
        public event EventHandler LocationTitleChanges;
        public string Name { get; set; }
        public string LocationTitle { get; private set; }
        public List<AppLink> NavLinks { get; private set; } = new List<AppLink>();
        public SettlementState SettlementState { get; private set; }

        

        public void SetNavLinks(List<AppLink> links) {
            NavLinks = links ?? new List<AppLink>() { };
            if(NavLinkChanges != null)
            {
                NavLinkChanges(null, EventArgs.Empty);
            }
        }

        public void SetUser(ClaimsPrincipal user)
        {

        }

        public void SetLocationTitle(string location)
        {
            LocationTitle = location;
            LocationTitleChanges(null, EventArgs.Empty);
        }

    }
}