using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;
using WanderingRetinue.Client.Interfaces;
using WanderingRetinue.Client.Models;
using WanderingRetinue.Client.Services;

namespace WanderingRetinue.Client.Pages.Settlement
{
    public class SettlementBase : ComponentBase
    {
        [Parameter]
        public int Id { get; set; }

        [Inject]
        public IAppState AppState { get; set; }

        public WanderingRetinue.Shared.Models.Settlement Model { get => AppState.SettlementState.Settlement; }

        protected override async Task OnInitializedAsync()
        {
            var token = await AppState.AuthStateProvider.GetTokenAsync();
            if (!string.IsNullOrEmpty(token))
            {
                var _client = await AppState.ClientBuilder.BuildODataClient();
                if (_client != null)
                {
                    await AppState.SettlementState.SetSettlement(Id);
                }
            }

            AppState.SetLocationTitle(Model.Name);
            AppState.SetNavLinks(new List<AppLink>()
            {
                new AppLink(null, "Settlement", new Uri($"/settlement/{Model.Id}"), AppLink.Location.Top, 0),
                new AppLink(null, "Survivors", new Uri($"/settlement/{Model.Id}/survivors"), AppLink.Location.Top, 1)
            });
        }

    }
}