﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Pages.Settlement.Components
{
    public class SettlementCardBase : ComponentBase
    {
        [Parameter]
        public WanderingRetinue.Shared.Models.Settlement Model { get; set; }

    }
}