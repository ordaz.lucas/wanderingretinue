﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;
using WanderingRetinue.Client.Interfaces;
using WanderingRetinue.Client.Models;
using WanderingRetinue.Client.Services;

namespace WanderingRetinue.Client.Pages.Settlement
{
    public class NewSettlementBase : ComponentBase
    {
        [Inject]
        public IAppState AppState { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        public WanderingRetinue.Shared.Models.Settlement Model { get; set; } = new WanderingRetinue.Shared.Models.Settlement();

        protected override async Task OnInitializedAsync()
        {
            var token = await AppState.AuthStateProvider.GetTokenAsync();
            if (!string.IsNullOrEmpty(token))
            {
                
            }
        }

        public async Task CreateSettlement()
        {
            var settlementService = await AppState.ClientBuilder.BuildRPCClient<WanderingRetinue.Shared.Interfaces.ISettlementService>();
            try
            {
                await settlementService.AddSettlement(Model);
                NavigationManager.NavigateTo("/");
            } catch(Exception)
            {
                
            }
            
        }

    }
}