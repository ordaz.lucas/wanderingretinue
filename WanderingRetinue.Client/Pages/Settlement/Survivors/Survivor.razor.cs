using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Pages
{
    public class SurvivorBase : ComponentBase
    {
        [Parameter]
        public int Id { get; set; }

        [Parameter]
        public int SurvivorId { get; set; }

        [Inject]
        protected IAppState AppState { get; set; }

        protected WanderingRetinue.Shared.Models.Survivor Model { get; set; }

    }
}