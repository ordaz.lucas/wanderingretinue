using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Pages.Settlement.Survivors
{
    public class SurvivorsBase : ComponentBase
    {
        [Parameter]
        public int Id { get; set; }

        [Inject]
        protected IAppState AppState { get; set; }

        protected WanderingRetinue.Shared.Models.Settlement Model { get; set; }

    }
}