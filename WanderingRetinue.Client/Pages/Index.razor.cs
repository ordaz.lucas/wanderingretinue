using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Pages
{
    public class IndexBase : ComponentBase
    {
        [Inject]
        public IAppState AppState { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        public IEnumerable<WanderingRetinue.Shared.Models.Settlement> Settlements { get; set; } = new List<WanderingRetinue.Shared.Models.Settlement>();

        public bool loadingData { get; set; } = false;

        protected override async Task OnInitializedAsync()
        {

            var token = await AppState.AuthStateProvider.GetTokenAsync();
            if(token != null)
            {
                AppState.SetNavLinks(new List<Models.AppLink>()
                {
                    new Models.AppLink(null, "Home", new Uri("/"), Models.AppLink.Location.Top, 1),
                    new Models.AppLink(null, "New Settlement", new Uri($"/settlement/new"), Models.AppLink.Location.Top, 2),
                    new Models.AppLink(null, "Logout", new Uri($"/logout"), Models.AppLink.Location.Top, 3),
                    new Models.AppLink(null, "Subscribe", () => RequestNotificationSubscriptionAsync(), Models.AppLink.Location.Top, 4)
                }); ;
                try
                {
                    loadingData = true;
                    var settlementService = await AppState.ClientBuilder.BuildRPCClient<WanderingRetinue.Shared.Interfaces.ISettlementService>();
                    var resp = await settlementService.GetSettlements();
                    if(resp != null)
                    {
                        Settlements = resp?.Settlements ?? new List<WanderingRetinue.Shared.Models.Settlement>();
                    }
                } catch(Exception e)
                {
                    
                } finally
                {
                    loadingData = false;
                }
                
            }
            
        }

        async Task RequestNotificationSubscriptionAsync()
        {

            Console.WriteLine("Called");
            var subscription = await JSRuntime.InvokeAsync<WanderingRetinue.Shared.Models.NotificationSubscription>("WanderingRetinue.requestSubscription");
            Console.WriteLine($"{subscription.Id}||{subscription.Auth}");
            if (subscription != null)
            {
                try
                {
                    Console.WriteLine("Called 2");
                    var notificationService = await AppState.ClientBuilder.BuildRPCClient<WanderingRetinue.Shared.Interfaces.INotificationService>();
                    await notificationService.Subscribe(subscription);
                    Console.WriteLine("Called 3");
                }
                catch (AccessTokenNotAvailableException ex)
                {
                    Console.WriteLine(ex.Message);
                    ex.Redirect();
                }
            }
        }

    }
}