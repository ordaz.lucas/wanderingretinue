﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.DTO;
using WanderingRetinue.Shared.Models;
using WanderingRetinue.Client.Services;
using Microsoft.AspNetCore.Components.Forms;
using WanderingRetinue.Client.AuthenticationStateProviders;
using System.Net.Http;
using Microsoft.AspNetCore.Components.Authorization;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Shared.Components
{
    public class RegisterBase : ComponentBase
    {
        [Inject]
        public TokenAuthenticationStateProvider AuthStateProvider { get; set; }

        [Inject]
        public HttpClient HttpClient { get; set; }

        protected UserAuthenticateRequest UserRegister { get; set; } = new UserAuthenticateRequest() { UserName = "admin", Password = "admin" };

        public EditForm RegisterForm { get; set; }

        [Inject]
        public IAppState AppState { get; set; }

        protected override async Task OnInitializedAsync()
        {
            AppState.SetLocationTitle("Register");
        }
        public async Task SubmitRegister()
        {
            var response = await HttpClient.PostAsJsonAsync("api/register", UserRegister).ConfigureAwait(false);
            var result = await response.Content.ReadAsAsync<UserAuthenticateResponse>().ConfigureAwait(false);
            if (!(result.Token == null))
            {
                await AuthStateProvider.SetTokenAsync(result.Token, DateTime.Today.AddDays(7)).ConfigureAwait(false);
            }
        }
    }
}
