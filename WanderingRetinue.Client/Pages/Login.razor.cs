﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.DTO;
using WanderingRetinue.Shared.Models;
using WanderingRetinue.Client.Services;
using Microsoft.AspNetCore.Components.Forms;
using WanderingRetinue.Client.AuthenticationStateProviders;
using System.Net.Http;
using Microsoft.AspNetCore.Components.Authorization;
using WanderingRetinue.Client.Interfaces;
using MudBlazor;

namespace WanderingRetinue.Client.Shared.Components
{
    public class LoginBase : ComponentBase
    {
        [Inject]
        public TokenAuthenticationStateProvider AuthStateProvider { get; set; }

        [Inject]
        public HttpClient HttpClient { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IAppState AppState { get; set; }

        protected UserAuthenticateRequest UserLogin { get; set; } = new UserAuthenticateRequest() { UserName = "admin", Password = "admin" };
        protected bool LoginFailure { get; set; }

        public MudForm LoginForm { get; set; }

        public async Task SubmitCredentials()
        {
            var userService = await AppState.ClientBuilder.BuildRPCClient<WanderingRetinue.Shared.Interfaces.IUserService>();
            var result = await userService.Authenticate(UserLogin);
            LoginFailure = result.Token == null;
            if (!LoginFailure)
            {
                await AuthStateProvider.SetTokenAsync(result.Token, DateTime.Today.AddDays(7)).ConfigureAwait(false);
            }
            string returnUri = NavigationManager.ToAbsoluteUri(NavigationManager.Uri).ParseQueryString().Get("returnUri");

            if(string.IsNullOrWhiteSpace(returnUri))
            {
                NavigationManager.NavigateTo("/");
            }
        }
    }
}
