﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WanderingRetinue.Shared.Models;
using WanderingRetinue.Client.Services;
using Microsoft.AspNetCore.Components.Forms;
using WanderingRetinue.Client.AuthenticationStateProviders;
using System.Net.Http;
using Microsoft.AspNetCore.Components.Authorization;
using WanderingRetinue.Client.Interfaces;

namespace WanderingRetinue.Client.Shared.Components
{
    public class ProfileBase : ComponentBase
    {
        [Inject]
        public TokenAuthenticationStateProvider AuthStateProvider { get; set; }

        [Inject]
        public HttpClient HttpClient { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IAppState AppState { get; set; }

        protected override async Task OnInitializedAsync()
        {
            AppState.SetLocationTitle(AppState.User.Identity.Name);
        }

    }
}
