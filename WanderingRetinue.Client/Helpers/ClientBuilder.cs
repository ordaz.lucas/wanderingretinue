﻿using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Web;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using ProtoBuf.Grpc.Client;
using Simple.OData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WanderingRetinue.Client.AuthenticationStateProviders;

namespace WanderingRetinue.Client.Helpers
{
    public class ClientBuilder
    {
        private GrpcChannel _grpcChannel { get; set; }
        public NavigationManager NavigationManager { get; private set; }
        public TokenAuthenticationStateProvider AuthStateProvider { get; private set; }
        private ODataClient _odataClient { get; set; }

        public ClientBuilder(NavigationManager navigationManager, TokenAuthenticationStateProvider tokenAuthentication)
        {
            NavigationManager = navigationManager;
            AuthStateProvider = tokenAuthentication;
        }

        public async Task<ODataClient> BuildODataClient()
        {
            if(_odataClient == null)
            {
                _odataClient = await RebuildODataClient();
            }

            return _odataClient;
        }

        public async Task<ODataClient> RebuildODataClient()
        {
            var token = await AuthStateProvider.GetTokenAsync();
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri($"{NavigationManager.BaseUri}odata");
            var settings = new ODataClientSettings(httpClient);
            settings.BeforeRequest += (HttpRequestMessage msg) =>
            {
                msg.Headers.Add("Authorization", $"Bearer {token}");
            };
            _odataClient = new ODataClient(settings);
            return await BuildODataClient();
        }

        public async Task<T> BuildRPCClient<T>() where T : class
        {
            if(_grpcChannel == null)
            {
                await RebuildGrpcChannel();
            }   
            
            return _grpcChannel.CreateGrpcService<T>();
        }

        public async Task<GrpcChannel> RebuildGrpcChannel()
        {
            var token = await AuthStateProvider.GetTokenAsync();
            var httpClient = new HttpClient(new GrpcWebHandler(GrpcWebMode.GrpcWeb, new HttpClientHandler()));
            if (!string.IsNullOrEmpty(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            var channel = GrpcChannel.ForAddress(NavigationManager.BaseUri, new GrpcChannelOptions { 
                HttpClient = httpClient
            });
            _grpcChannel = channel;
            return channel;
        }
    }
}
