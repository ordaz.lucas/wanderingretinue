﻿using ProtoBuf.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.Interfaces
{
    [ServiceContract]
    public interface INotificationService
    {
        Task Subscribe(Shared.Models.NotificationSubscription subscription, CallContext context = default);
    }
}
