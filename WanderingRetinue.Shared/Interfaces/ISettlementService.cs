﻿using ProtoBuf.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.Interfaces
{
    
    [ServiceContract]
    public interface ISettlementService
    {
        Task<DTO.GetSettlementsReply> GetSettlements(CallContext context = default);
        Task AddSettlement(Models.Settlement settlement, CallContext context = default);
        Task UpdateSettlement(Models.Settlement settlement, CallContext context = default);
        Task DeleteSettlement(Models.Settlement settlement, CallContext context = default);
    }
}
