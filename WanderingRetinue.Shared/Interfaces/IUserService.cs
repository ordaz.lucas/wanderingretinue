﻿using ProtoBuf.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.Interfaces
{
    [ServiceContract]
    public interface IUserService
    {
        Task<DTO.UserAuthenticateResponse> Authenticate(DTO.UserAuthenticateRequest req, CallContext context = default);
        Task<DTO.UserAuthenticateResponse> AuthenticateUserByToken(DTO.UserAuthenticateRequest req, CallContext context = default);
        Task<Models.User> GetUser(Models.User user, CallContext context = default);
        Task<DTO.UserAuthenticateResponse> Register(DTO.UserAuthenticateRequest req, CallContext context = default);
        Task Logout(CallContext context = default);
    }
}
