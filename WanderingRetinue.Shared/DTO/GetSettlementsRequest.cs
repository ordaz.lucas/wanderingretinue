﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.DTO
{
    [DataContract]
    public class GetSettlementsRequest
    {
        [DataMember(Order = 1)]
        public int UserId { get; set; }
    }
}
