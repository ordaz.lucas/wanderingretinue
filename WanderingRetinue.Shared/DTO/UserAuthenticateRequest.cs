﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WanderingRetinue.Shared.DTO
{
    [DataContract]
    public class UserAuthenticateRequest
    {
        [DataMember(Order = 1)]
        public string UserName { get; set; }
        [DataMember(Order = 2)]
        public string Password { get; set; }
        [DataMember(Order = 3)]
        public string Email { get; set; }
    }
}
