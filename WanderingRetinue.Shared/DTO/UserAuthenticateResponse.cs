﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WanderingRetinue.Shared.DTO
{
    [DataContract]
    public class UserAuthenticateResponse
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string UserName { get; set; }
        [DataMember(Order = 3)]
        public string Token { get; set; }
        public Shared.Models.User User { get; set; }
    }
}
