﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.DTO
{
    [DataContract]
    public class GetSettlementsReply
    {
        [DataMember(Order = 1)]
        public List<Models.Settlement> Settlements { get; set; }
    }
}
