using System;

namespace WanderingRetinue.Shared.Models
{
    public class Item
    {
        public enum ItemType 
        {
            Resource,
            Equipment
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemType Type { get; set;}
    }
}
