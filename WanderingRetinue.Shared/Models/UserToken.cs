﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.Models
{
    public class UserToken
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public byte[] TokenHash { get; set; }
        public byte[] TokenSalt { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
