﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class UserRegister
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}
