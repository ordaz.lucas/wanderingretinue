﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementNemesis
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int NemesisId { get; set; }
        public Nemesis Nemesis { get; set; }
        public int CurrentLevel { get; set; }
    }
}
