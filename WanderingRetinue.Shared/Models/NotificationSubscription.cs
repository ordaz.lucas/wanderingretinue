﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WanderingRetinue.Shared.Models
{
    [DataContract]
    public class NotificationSubscription
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int UserId { get; set; }

        [DataMember(Order = 3)]
        public string Url { get; set; }

        [DataMember(Order = 4)]
        public string P256dh { get; set; }

        [DataMember(Order = 5)]
        public string Auth { get; set; }
    }
}
