﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementMonster
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int MonsterId { get; set; }
        public Monster Monster { get; set; }
        public bool Unlocked { get; set; }
        public int MaxLevel { get; set; }
    }
}
