﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementPrinciple
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int PrincipleId { get; set; }
        public Principle Principle { get; set; }
        public bool Unlocked { get; set; }
    }
}
