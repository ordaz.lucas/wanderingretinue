﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementLocation
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int LocationId { get; set; }
        public Location Location { get; set; }
        public bool Unlocked { get; set; }
    }
}
