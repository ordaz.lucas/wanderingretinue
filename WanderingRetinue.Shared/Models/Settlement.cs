using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace WanderingRetinue.Shared.Models
{
    /// <summary>
    /// The Settlement
    /// </summary>
    [DataContract]
    public class Settlement
    {
        /// <summary>
        /// Unique Settlement Id
        /// </summary>
        [DataMember(Order = 1)]
        public int Id { get; set; }
        /// <summary>
        /// The Settlement's Name
        /// </summary>
        [DataMember(Order = 2)]
        public string Name { get; set; }
        /// <summary>
        /// Brief description of the settlement
        /// </summary>
        [DataMember(Order = 3)]
        public string Description { get; set; }
        /// <summary>
        /// The current Survival Limit
        /// </summary>
        [DataMember(Order = 4)]
        public int SurvivalLimit { get; set; }
        /// <summary>
        /// The last Lantern Year
        /// </summary>
        [DataMember(Order = 5)]
        public int MaxLaternYear { get; set; }
        /// <summary>
        /// The current Latern Year
        /// </summary>
        [DataMember(Order = 6)]
        public int LanternYear { get; set; }
        /// <summary>
        /// How many survivors has perished
        /// </summary>
        [DataMember(Order = 7)]
        public int DeathCount
        {
            get 
            {
                return Survivors?.Count(s => s.Dead) ?? 0;
            }
            set
            {
                //
            }
        }
        /// <summary>
        /// How many survivors are currently alive
        /// </summary>
        [DataMember(Order = 8)]
        public int Population
        {
            get
            {
                return Survivors?.Count(s => !s.Dead) ?? 0;
            }
            set
            {

            }
        }

        [DataMember(Order = 9)]
        public List<SettlementUser> Users { get; set; }
        [DataMember(Order = 10)]
        public List<SettlementSurvivalAction> SurvivalActions { get; set; }
        [DataMember(Order = 11)]
        public List<SettlementNemesis> Nemesis { get; set; }
        [DataMember(Order = 12)]
        public List<SettlementStoryEvent> StoryEvents { get; set; }
        [DataMember(Order = 13)]
        public List<SettlementLanternEvent> LanternEvents { get; set; }
        [DataMember(Order = 14)]
        public List<Survivor> Survivors { get; set; }
        [DataMember(Order = 15)]
        public List<SettlementLocation> Locations { get; set; }
        [DataMember(Order = 16)]
        public List<SettlementPrinciple> Principles { get; set; }
        [DataMember(Order = 17)]
        public List<SettlementMonster> Monsters { get; set; }
        [DataMember(Order = 18)]
        public List<SettlementItem> Items { get; set; }
    }
}
