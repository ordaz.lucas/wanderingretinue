﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    [DataContract]
    public class SettlementUser
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public int SettlementId { get; set; }
        [DataMember(Order = 3)]
        public int UserId { get; set; }
    }
}
