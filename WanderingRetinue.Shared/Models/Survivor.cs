using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace WanderingRetinue.Shared.Models
{
    [DataContract]
    public class Survivor
    {

        [DataContract]
        public enum SexEnum 
        {
            [EnumMember]
            None = 0,
            [EnumMember]
            Male = 1,
            [EnumMember]
            Female = 2
        }
        [DataContract]
        public enum CourageEnum
        {
            [EnumMember]
            None = 0,
            [EnumMember]
            Stalwart = 1,
            [EnumMember]
            Prepared = 2,
            [EnumMember]
            Matchmaker = 3
        }
        [DataContract]
        public enum UnderstandingEnum
        {
            [EnumMember]
            None = 0,
            [EnumMember]
            Analyze = 1,
            [EnumMember]
            Explore = 2,
            [EnumMember]
            Tinker = 3
        }
        [DataContract]
        public enum WeaponProficiencyEnum
        {
            [EnumMember]
            None = 0,
            [EnumMember]
            Specialist = 1,
            [EnumMember]
            Master = 2
        }

        [DataMember(Order = 1)]
        public int? Id { get; set; }
        [DataMember(Order = 2)]
        public int? SettlementId { get; set; }
        [DataMember(Order = 3)]
        public string Name { get; set; }
        [DataMember(Order = 4)]
        public bool Dead { get; set; }
        [DataMember(Order = 5, EmitDefaultValue = true), DefaultValue(SexEnum.None)]
        public SexEnum Sex { get; set; }
        [DataMember(Order = 6)]
        public int Survival { get; set; }
        [DataMember(Order = 7)]
        public bool CannotUseSurvial { get; set; }
        [DataMember(Order = 8)]
        public int Movement { get; set; }
        [DataMember(Order = 9)]
        public int Accuracy { get; set; }
        [DataMember(Order = 10)]
        public int Strength { get; set; }
        [DataMember(Order = 11)]
        public int Evasion { get; set; }
        [DataMember(Order = 12)]
        public int Luck { get; set; }
        [DataMember(Order = 13)]
        public int Speed { get; set; }
        [DataMember(Order = 14)]
        public int Insanity { get; set; }
        [DataMember(Order = 15)]
        public bool Insane
        {
            get
            {
                return Insanity >= 3;
            }
            set
            {

            }
        }
        [DataMember(Order = 16)]
        public int WeaponProficiency { get; set; }
        [DataMember(Order = 17, EmitDefaultValue = true), DefaultValue(WeaponProficiencyEnum.None)]
        public WeaponProficiencyEnum WeaponProficiencyMaster { get; set;}
        [DataMember(Order = 18)]
        public int Courage { get; set; }
        [DataMember(Order = 19, EmitDefaultValue = true), DefaultValue(CourageEnum.None)]
        public CourageEnum CourageProperty { get; set; }
        [DataMember(Order = 20)]
        public int Understanding { get; set; }
        [DataMember(Order = 21, EmitDefaultValue = true), DefaultValue(UnderstandingEnum.None)]
        public UnderstandingEnum UnderstandingProperty { get; set; }
        [DataMember(Order = 22)]
        public bool CannotUseFightingArts { get; set; }
        [DataMember(Order = 23)]
        public bool SkipNextHunt { get; set; }
        //public List<FightingArt> FightingArts { get; set; }
        //public List<Disorder> Disorders { get; set; }
        //public List<Impairment> Impairments { get; set; }
        
    }
}
