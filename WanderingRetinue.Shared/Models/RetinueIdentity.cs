﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class RetinueIdentity : ClaimsIdentity
    {
        public User User { get; private set; }
        public RetinueIdentity(User user, IEnumerable<Claim> claims)
        {
            User = user;
            if(claims != null)
            {
                AddClaims(claims);
            }
        } 
        public override string Name => User?.UserName ?? "Anonymous";

        public override string AuthenticationType => "Retinue";

        public override bool IsAuthenticated => User != null;

        public static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
            return keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value?.ToString()));
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }

    }
}
