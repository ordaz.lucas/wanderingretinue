using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementItem
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public int Qty { get; set; }
    }
}
