using System;

namespace WanderingRetinue.Shared.Models
{
    public class Disorder
    {
        public string Name { get; set; }
    }
}
