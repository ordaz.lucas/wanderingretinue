using System;
using System.ComponentModel.DataAnnotations;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementStoryEvent
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int StoryEventId { get; set; }
        public StoryEvent StoryEvent { get; set; }
        public bool Reached { get; set; }
    }
}
