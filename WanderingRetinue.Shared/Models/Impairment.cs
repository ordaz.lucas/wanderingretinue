using System;

namespace WanderingRetinue.Shared.Models
{
    public class Impairment
    {
        public string Name { get; set; }
    }
}
