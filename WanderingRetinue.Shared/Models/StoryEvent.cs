﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class StoryEvent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
