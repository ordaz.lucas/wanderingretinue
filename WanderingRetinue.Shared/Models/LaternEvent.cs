using System;

namespace WanderingRetinue.Shared.Models
{
    public class LanternEvent
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
