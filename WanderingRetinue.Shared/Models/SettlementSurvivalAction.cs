using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WanderingRetinue.Shared.Models
{
    [DataContract]
    public class SettlementSurvivalAction
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public int SettlementId { get; set; }
        [DataMember(Order = 3)]
        public int SurvivalActionId { get; set; }
        [DataMember(Order = 4)]
        public bool Unlocked { get; set; }
    }
}
