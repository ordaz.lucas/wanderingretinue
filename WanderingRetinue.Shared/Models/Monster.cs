using System;

namespace WanderingRetinue.Shared.Models
{
    public class Monster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
