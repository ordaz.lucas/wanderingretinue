﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WanderingRetinue.Shared.Models
{
    public class SettlementLanternEvent
    {
        public int Id { get; set; }
        public int SettlementId { get; set; }
        public Settlement Settlement { get; set; }
        public int LanternEventId { get; set; }
        public LanternEvent LaternEvent { get; set; }
        public int LaternYear { get; set; }
        public bool Completed { get; set; }

    }
}
